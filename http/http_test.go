package http_test

import (
	"github.com/stretchr/testify/assert"
	sut "gitlab.com/djoodle/go-inators/http"
	"testing"
	"time"
)

func TestCap(t *testing.T) {
	c := sut.NewDefault()
	assert.NotNil(t, c)
	c = sut.NewWithTimeout(20 * time.Second)
	assert.NotNil(t, c)
}
