<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [http](#http)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# http
An opinionated default `net/http` client.
