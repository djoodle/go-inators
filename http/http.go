package http

import (
	"net"
	gohttp "net/http"
	"time"
	"math"
)

const (
	DefaultTimeout = 10 * time.Second
)

// NewDefault returns a `*http.Client` with a default 10
// second total timeout
func NewDefault() *gohttp.Client {
	return NewWithTimeout(DefaultTimeout)
}

// NewWithTimeout returns a `*http.Client` with timeouts
// based on a custom total timeout.
func NewWithTimeout(to time.Duration) *gohttp.Client {
	return &gohttp.Client{
		Transport: &gohttp.Transport{
			DialContext: (&net.Dialer{
				Timeout:   to,
				KeepAlive: to,
			}).DialContext,
			TLSHandshakeTimeout:   to,
			ExpectContinueTimeout: to - time.Duration(math.Max(float64(to-DefaultTimeout)/2, 0))*time.Second,
			ResponseHeaderTimeout: to - time.Duration(math.Max(float64(to-DefaultTimeout)/3, 0))*time.Second,
		},
		// Prevent endless redirects
		Timeout: 1 * time.Minute,
	}
}
