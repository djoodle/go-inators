#!/bin/bash

apt-get update && apt-get install -y --no-install-recommends apt-transport-https

# Add apt key for LLVM repository
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-5.0 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository
apt-get update && apt-get install -y --no-install-recommends \
    clang-6.0 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
set_clang=/etc/profile.d/set-clang-cc.sh
echo "export CC=clang-6.0" | tee -a ${set_clang} && chmod a+x ${set_clang}
