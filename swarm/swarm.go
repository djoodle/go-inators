package swarm

import (
	"context"
	"sync"
)

// Task describes the type of functions we can operate over.
// Task functions take two arguments, idx is the index of the task
// based, and routine is the index of the routine that is running
// it.
type Task func(idx, routine int) error

// TaskWithContext is the same as a Task, but with a context passed
// in for control.
type TaskWithContext func(ctx context.Context, idx, routine int) error

// Do runs a provided `Task`, t, `count` number of times, using `routines`
// number of goroutines.
func Do(count, routines int, t Task) error {
	ctxWithCncl, cncl := context.WithCancel(context.Background())
	defer cncl()

	var wg sync.WaitGroup
	wg.Add(routines)
	errCh := make(chan error, routines)
	done := make(chan struct{})
	for i := 0; i < routines; i++ {
		s, e := count/routines*i, count/routines*(i+1)
		if i == routines-1 {
			e = count
		}
		go func(i, s, e int) {
			defer wg.Done()
			for j := s; j < e; j++ {
				select {
				case <-ctxWithCncl.Done():
					return
				default:
					err := t(j, i)
					if err != nil {
						errCh <- err
					}
				}
			}
		}(i, s, e)
	}

	// signal when wg is done
	go func() {
		wg.Wait()
		close(done)
	}()

	// wait until either WaitGroup is done or an error is received through the channel
	var err error
	for {
		select {
		case <-done:
			close(errCh)
			return err
		case e := <-errCh:
			if err == nil {
				err = e
			}
			cncl()
		}
	}
}

// DoWithContext is the same as `Do` but takes a Context and checks to
// see if it is done before starting each Task. The Context is also then
// passed to the Task for controlling long operations.
func DoWithContext(ctx context.Context, count, routines int, t TaskWithContext) error {
	ctxWithCncl, cncl := context.WithCancel(ctx)
	defer cncl()

	var wg sync.WaitGroup
	wg.Add(routines)
	done := make(chan struct{})
	errCh := make(chan error, routines)
	for i := 0; i < routines; i++ {
		s, e := count/routines*i, count/routines*(i+1)
		if i == routines-1 {
			e = count
		}

		go func(i, s, e int) {
			defer wg.Done()
			for j := s; j < e; j++ {
				select {
				case <-ctxWithCncl.Done():
					return
				default:
					err := t(ctxWithCncl, j, i)
					if err != nil {
						errCh <- err
					}
				}
			}
		}(i, s, e)
	}

	// signal when wg is done
	go func() {
		wg.Wait()
		close(done)
	}()

	// wait until either WaitGroup is done or an error is received through the channel
	var err error
	for {
		select {
		case <-done:
			close(errCh)
			return err
		case e := <-errCh:
			cncl()
			if err == nil {
				err = e
			}
		}
	}
}
