<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [swarm](#swarm)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# swarm
A simple library to run a number of goroutines to
swarm on a task. Handles the first error returned
