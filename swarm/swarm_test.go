package swarm_test

import (
	"context"
	"errors"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
	sut "gitlab.com/djoodle/go-inators/swarm"
)

func TestDo(t *testing.T) {
	t.Parallel()
	t.Run(
		"10 iterations on 4 routines",
		func(tt *testing.T) {
			var count int64
			_ = sut.Do(
				10, 4,
				func(_, _ int) error {
					atomic.AddInt64(&count, 1)
					return nil
				},
			)
			assert.Equal(tt, int64(10), count)
		},
	)

	t.Run(
		"returns the error thrown by the task",
		func(tt *testing.T) {
			err := sut.Do(10, 4, func(_, _ int) error { return errors.New("fail") })
			assert.Error(tt, err)
			assert.EqualError(tt, err, "fail")
		},
	)
}

func TestDoWithContext(t *testing.T) {
	t.Parallel()
	t.Run(
		"context is not cancelled",
		func(tt *testing.T) {
			tt.Parallel()

			var count int64
			_ = sut.DoWithContext(
				context.Background(),
				10, 4,
				func(_ context.Context, _, _ int) error {
					atomic.AddInt64(&count, 1)
					return nil
				},
			)
			assert.Equal(tt, int64(10), count)
		},
	)

	t.Run(
		"context is cancelled",
		func(tt *testing.T) {
			tt.Parallel()

			var count int64
			ctx, cancel := context.WithCancel(context.Background())

			_ = sut.DoWithContext(
				ctx,
				10, 1,
				func(_ context.Context, _, _ int) error {
					atomic.AddInt64(&count, 1)
					cancel()
					return nil
				},
			)
			assert.Equal(tt, int64(1), count)
		},
	)

	t.Run(
		"returns the error thrown by the task",
		func(tt *testing.T) {
			err := sut.DoWithContext(
				context.Background(), 10, 4,
				func(ctx context.Context, _, _ int) error { return errors.New("fail") },
			)
			assert.Error(tt, err)
			assert.EqualError(tt, err, "fail")
		},
	)
}
