<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [go-inators * * * *](#go-inators----)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# go-inators [![Pipeline Status](https://gitlab.com/djoodle/go-inators/badges/master/pipeline.svg)](https://gitlab.com/djoodle/go-inators/commits/master) [![Coverage Report](https://gitlab.com/djoodle/go-inators/badges/master/coverage.svg)](https://gitlab.com/djoodle/go-inators/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/djoodle/go-inators)](https://goreportcard.com/report/gitlab.com/djoodle/go-inators) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
A bunch of inators to be used in various projects, written in go
