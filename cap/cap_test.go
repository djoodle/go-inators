package cap_test

import (
	"context"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/benbjohnson/clock"
	"github.com/stretchr/testify/assert"

	sut "gitlab.com/djoodle/go-inators/cap"
)

func TestCap(t *testing.T) {
	c := sut.New(5)
	var wg sync.WaitGroup
	var count, atomicFail int64

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for i := 0; i < 1000; i++ {
		select {
		case <-ctx.Done():
			assert.Equal(t, int64(0), atomicFail)
		default:
			wg.Add(1)
			go func() {
				defer wg.Done()
				c.Begin()
				defer c.End()
				n := atomic.AddInt64(&count, 1)
				defer atomic.AddInt64(&count, -1)
				if n < 0 || n > 5 {
					atomic.StoreInt64(&atomicFail, 1)
					cancel()
				}
			}()
		}
	}
	wg.Wait()
}

func TestRateLimiter(t *testing.T) {
	c := clock.NewMock()
	rl := sut.NewLimiter(c, 100, time.Second)

	var wg sync.WaitGroup
	var count int64

	// Create copious counts concurrently.
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			rl.Begin()
			defer rl.End()
			atomic.AddInt64(&count, 1)
		}()
	}

	for i := int64(0); i < 100; i++ {
		c.Add(10 * time.Millisecond)
		assert.LessOrEqual(t, i, atomic.LoadInt64(&count), "count within rate limit")
	}
	wg.Wait()
}
