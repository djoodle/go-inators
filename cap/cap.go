package cap

import (
	"sync/atomic"
	"time"
	"unsafe"
)

type (
	IFace interface {
		Begin()
		End()
	}

	simplecap struct {
		signal chan struct{}
	}

	ratelimitcap struct {
		state  unsafe.Pointer
		perReq time.Duration
		clock  Clock
	}

	ratelimitstate struct {
		last  time.Time
		sleep time.Duration
	}

	Clock interface {
		Now() time.Time
		Sleep(time.Duration)
	}
)

// New returns a new Cap. The limit param is the maximum number of
// concurrent tasks.
func New(size int) IFace {
	return &simplecap{make(chan struct{}, size)}
}

// NewLimiter returns a new Cap. This cap uses a leaky
// bucket rate limiter, rather than simple concurrent
// blocking.
func NewLimiter(clock Clock, rate int, interval time.Duration) IFace {
	c := &ratelimitcap{
		perReq: interval / time.Duration(rate),
		clock:  clock,
	}

	init := ratelimitstate{
		sleep: 0,
		last:  time.Time{},
	}

	atomic.StorePointer(&c.state, unsafe.Pointer(&init))
	return c
}

// Begin is used to signify our intent begin a task. This
// method will block until their is room within the cap.
func (c *simplecap) Begin() {
	c.signal <- struct{}{}
}

// End signifies when we are done with the task and
// can free up our cap.
func (c *simplecap) End() {
	<-c.signal
}

// Begin is used to signify our internt to begin a task. This
// method will block until there is room in the bucket.
func (c *ratelimitcap) Begin() {
	var state ratelimitstate
	begun := false
	for !begun {
		now := c.clock.Now()
		prevPtr := atomic.LoadPointer(&c.state)
		old := (*ratelimitstate)(prevPtr)
		state = ratelimitstate{}
		state.last = now

		// Check if this is the first request
		if old.last.IsZero() {
			begun = atomic.CompareAndSwapPointer(&c.state, prevPtr, unsafe.Pointer(&state))
			continue
		}

		state.sleep += c.perReq - now.Sub(old.last)
		if state.sleep > 0 {
			state.last = state.last.Add(state.sleep)
		}
		begun = atomic.CompareAndSwapPointer(&c.state, prevPtr, unsafe.Pointer(&state))
	}
	c.clock.Sleep(state.sleep)
}

// End is a no-op. Rate limiter doesn't need to signify end
// capping based on requests not long running actions
func (*ratelimitcap) End() {}
