<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [canal](#canal)
  - [Interface](#interface)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# canal

A simple interface and wrapper around go-channles I always seem
to recreate whatever I am doing.

## Interface
```go
type IFace interface {
	// Send a message to the cannal. Returns false if the canal is closed.
	Send(v interface{}) (ok bool)
	// Recv receives a message off the canal. Returns false if the canal is closed.
	Recv() (v interface{}, ok bool)
	// Close the canal. Returns false if the canal is already closed.
	Close() (ok bool)
	// Wait for the canal to close. Returns immediately if the canal is
	// already closed
	Wait()
	// Merge will merge the current canal with each of the passed in canals
	// creating a new canal. Returns false if any canal is closed. This
	// allows for `fan-in`
	Merge(threads int, cs ...IFace) (merged IFace)
	// Join drains a second canal onto the current canal. Returns false if the
	// first canal is closed before the second is fully drained.
	Join(c2 IFace) (ok bool)
	// Len returns the length (number of boats currently on) the canal
	Len() int
	// Closed returns true if the canal has been closed, false if not.
	Closed() bool
	// Unload unloads the boats from the canal, running an optional function against
	// them.
	Unload(fn func(interface{}, bool))
	// UnloadWithContext is the same as unload excepts it checks that a context
	// hasn't been closed before processing. Returns false if the context is closed
	// before the canal.
	UnloadWithContext(ctx context.Context, fn func(context.Context, interface{}, bool)) (ok bool)
}
```
