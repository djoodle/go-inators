// Package canal is an interface and common functions around
// go channels. Nothing all that complicated here.
package canal

import (
	"context"
	"sync"

	"gitlab.com/djoodle/go-inators/swarm"
)

type boat struct {
	v  interface{}
	ok bool
}

// IFace is the interface to use
type IFace interface {
	// Send a message to the cannal. Returns false if the canal is closed.
	Send(v interface{}) (ok bool)
	// Recv receives a message off the canal. Returns false if the canal is closed.
	Recv() (v interface{}, ok bool)
	// Close the canal. Returns false if the canal is already closed.
	Close() (ok bool)
	// Wait for the canal to close. Returns immediately if the canal is
	// already closed
	Wait()
	// Merge will merge the current canal with each of the passed in canals
	// creating a new canal. Returns false if any canal is closed. This
	// allows for `fan-in`
	Merge(threads int, cs ...IFace) (merged IFace)
	// Join drains a second canal onto the current canal. Returns false if the
	// first canal is closed before the second is fully drained.
	Join(c2 IFace) (ok bool)
	// Len returns the length (number of boats currently on) the canal
	Len() int
	// Closed returns true if the canal has been closed, false if not.
	Closed() bool
	// Unload unloads the boats from the canal, running an optional function against
	// them.
	Unload(fn func(interface{}, bool))
	// UnloadWithContext is the same as unload excepts it checks that a context
	// hasn't been closed before processing. Returns false if the context is closed
	// before the canal.
	UnloadWithContext(ctx context.Context, fn func(context.Context, interface{}, bool)) (ok bool)
}

type canal struct {
	ch     chan boat
	mu     sync.Mutex
	cond   *sync.Cond
	closed bool
}

// Make returns a new canal of length `length`
func Make(length int) IFace {
	c := &canal{ch: make(chan boat, length)}
	c.cond = sync.NewCond(&c.mu)
	return c
}

func (c *canal) Send(v interface{}) (ok bool) {
	defer func() { ok = recover() == nil }()
	c.ch <- boat{v, true}
	return
}

func (c *canal) Recv() (v interface{}, ok bool) {
	boat := <-c.ch
	return boat.v, boat.ok
}

func (c *canal) Close() (ok bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	defer func() { ok = recover() == nil }()
	close(c.ch)
	c.closed = true
	c.cond.Broadcast()
	return
}

func (c *canal) Closed() bool {
	return c.closed
}

func (c *canal) Join(c2 IFace) (ok bool) {
	for !c.closed {
		v, ok := c2.Recv()
		if !ok {
			return true
		}
		c.Send(v)
	}
	return false
}

func (c *canal) Len() int {
	return len(c.ch)
}

func (c *canal) Wait() {
	c.mu.Lock()
	defer c.mu.Unlock()
	for {
		if c.closed {
			return
		}
		c.cond.Wait()
	}
}

func (c *canal) Merge(threads int, cs ...IFace) (merged IFace) {
	cs = append(cs, c)
	var boats int
	for _, cc := range cs {
		boats += cc.Len()
	}
	merged = Make(boats)
	_ = swarm.Do(len(cs), threads, func(i, _ int) error {
		merged.Join(cs[i])
		return nil
	})

	return merged
}

func (c *canal) Unload(fn func(interface{}, bool)) {
	for {
		boat := <-c.ch
		if !boat.ok {
			return
		}

		fn(boat.v, boat.ok)
	}
}

func (c *canal) UnloadWithContext(ctx context.Context, fn func(context.Context, interface{}, bool)) bool {
	for {
		select {
		case <-ctx.Done():
			return false
		case boat := <-c.ch:
			if !boat.ok {
				return true
			}
			fn(ctx, boat.v, boat.ok)
		}
	}
}

var _ IFace = &canal{}
