package canal_test

import (
	"context"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	sut "gitlab.com/djoodle/go-inators/canal"
	"gitlab.com/djoodle/go-inators/swarm"
)

func TestCanal(t *testing.T) {
	t.Parallel()
	t.Run(
		"can send and receive a value down the canal",
		func(tt *testing.T) {
			tt.Parallel()

			c := sut.Make(5)
			defer c.Close()

			c.Send("test")
			assert.Equal(tt, c.Len(), 1)

			v, ok := c.Recv()
			assert.True(tt, ok)
			assert.Equal(tt, v, "test")
			closed := c.Close()
			assert.True(tt, closed)
		},
	)

	t.Run(
		"Len returns the number of boats on the canal",
		func(tt *testing.T) {
			tt.Parallel()

			c := sut.Make(15)
			_ = swarm.Do(10, 1, func(_, _ int) error {
				c.Send(struct{}{})
				return nil
			})
			assert.Equal(tt, 10, c.Len())

			_ = swarm.Do(5, 1, func(_, _ int) error {
				_, ok := c.Recv()
				if !ok {
					tt.Error("the canal shouldn't be closed")
				}
				return nil
			})
			assert.Equal(tt, 5, c.Len())
			closed := c.Close()
			assert.True(tt, closed)
		},
	)
}

func TestCanalJoin(t *testing.T) {
	t.Parallel()
	t.Run(
		"can join one canal to another",
		func(tt *testing.T) {
			tt.Parallel()

			c := sut.Make(10)
			cc := sut.Make(5)

			_ = swarm.Do(5, 1, func(_, _ int) error {
				c.Send(1)
				return nil
			})
			assert.Equal(tt, 5, c.Len())
			_ = swarm.Do(5, 1, func(_, _ int) error {
				cc.Send(2)
				return nil
			})
			assert.Equal(tt, 5, cc.Len())
			cc.Close()

			ok := c.Join(cc)
			assert.True(tt, ok)
			assert.Equal(tt, 10, c.Len())

			var ones, twos int
			for c.Len() != 0 {
				switch v, _ := c.Recv(); v {
				case 1:
					ones++
				case 2:
					twos++
				default:
					tt.Error("unexpected value pulled from a boat")
				}
			}

			assert.Equal(tt, 5, ones)
			assert.Equal(tt, 5, twos)
			closed := c.Close()
			assert.True(tt, closed)
		},
	)

	t.Run(
		"returns true if the second channel is closed",
		func(tt *testing.T) {
			tt.Parallel()

			c := sut.Make(1)
			defer c.Close()
			cc := sut.Make(1)
			cc.Close()
			ok := c.Join(cc)

			assert.True(tt, ok)
		},
	)

	t.Run(
		"returns false if the first channel is closed before we start",
		func(tt *testing.T) {
			tt.Parallel()
			c := sut.Make(1)
			c.Close()
			cc := sut.Make(1)
			defer cc.Close()
			ok := c.Join(cc)
			assert.False(tt, ok)
		},
	)
}

func TestCanalMerge(t *testing.T) {
	t.Parallel()

	c := sut.Make(1)
	c.Send(1)
	c.Close()
	cc := sut.Make(1)
	cc.Send(2)
	cc.Close()
	ccc := sut.Make(1)
	ccc.Send(3)
	ccc.Close()

	c4 := c.Merge(3, cc, ccc)
	assert.Equal(t, 3, c4.Len())
	c4.Close()

	var total int
	for {
		var v interface{}
		var ok bool
		if v, ok = c4.Recv(); !ok {
			break
		}
		total += v.(int)
	}

	assert.Equal(t, 6, total)
}

func TestCanalClose(t *testing.T) {
	t.Parallel()

	c := sut.Make(1)
	assert.False(t, c.Closed())
	c.Close()
	assert.True(t, c.Closed())
}

func TestCanalWait(t *testing.T) {
	t.Parallel()
	var count int64
	var wg sync.WaitGroup
	c := sut.Make(5)
	wg.Add(1)
	go func() {
		defer wg.Done()
		time.Sleep(100 * time.Millisecond)
		atomic.StoreInt64(&count, 1)
		c.Close()
	}()

	c.Wait()
	assert.Equal(t, int64(1), count)
	wg.Wait()
}

func TestCanalUnload(t *testing.T) {
	var count int
	t.Parallel()
	c := sut.Make(3)
	c.Send(struct{}{})
	c.Send(struct{}{})
	c.Send(struct{}{})
	c.Close()

	c.Unload(func(_ interface{}, _ bool) {
		count++
	})

	assert.Equal(t, 3, count)
}

func TestCanalUnloadWithContext(t *testing.T) {
	t.Parallel()
	t.Run("context is cancelled", func(tt *testing.T) {
		tt.Parallel()
		var count int
		c := sut.Make(3)
		defer c.Close()
		ctx, cncl := context.WithCancel(context.Background())
		c.Send(struct{}{})

		ok := c.UnloadWithContext(ctx, func(_ context.Context, _ interface{}, _ bool) {
			count++
			cncl()
		})

		assert.Equal(tt, 1, count)
		assert.False(tt, ok)
	})

	t.Run("channel is cancelled", func(tt *testing.T) {
		tt.Parallel()
		var count int
		c := sut.Make(3)
		c.Send(struct{}{})
		c.Send(struct{}{})
		c.Send(struct{}{})
		c.Close()

		ok := c.UnloadWithContext(context.Background(), func(_ context.Context, _ interface{}, _ bool) {
			count++
		})

		assert.Equal(tt, 3, count)
		assert.True(tt, ok)
	})

}
