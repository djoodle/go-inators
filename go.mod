module gitlab.com/djoodle/go-inators

go 1.14

require (
	github.com/benbjohnson/clock v1.0.3
	github.com/stretchr/testify v1.6.1
)
